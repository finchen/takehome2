const { Router } = require('express');
const httperrors = require('httperrors');
const Thing = require('../models/thing');
const pagination = require('../lib/pagination');
const config = require('../../config');

module.exports = new Router()
  .get('', async (req, res) => {
    const query = pagination(req.query);
    const results = await Thing.findAll(query);
    res.json(results);
  })
  .get('/:id', async (req, res) => {
    const thing = await Thing.findById(req.params.id);

    if (!thing) throw httperrors.NotFound();

    res.json(thing);
  })
  .post('', async (req, res) => {
    const payload = req.body;

    if (payload.quantity < 10) throw httperrors.BadRequest();

    // if not admin, check the metadata for danger
    if ('name' in payload && req.auth.pass !== config.ADMIN_PASS) {
      const metadatas = payload.name.split(' ');
      metadatas.forEach((metadata) => {
        const data = metadata.split(':');
        if (data.length === 3) {
          // metadata. not name
          const isDanger = data[1] === 'true';
          if (isDanger) throw httperrors.Forbidden();
        }
      });

    }
    const created = await Thing.create(payload);

    res.json(created);
  });
